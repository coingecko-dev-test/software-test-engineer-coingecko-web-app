# Software Test Engineer - Coingecko Web App

# CoinGecko Software Test Engineer - Basic User Journey Asssessment

You are tasked to develop a simple Test Plan for a basic user journey on the Coingecko web app: [https://www.coingecko.com](https://www.coingecko.com)


## Product Specifications


## User Signup and Portfolio

1. A Guest is required to provide Email and Password to Sign Up. 
2. An authenticated user (_"User"_) is able to access a default Portfolio ("My Portfolio")
3. A User is able to add new coins to their portfolio
4. A User is able to Add Transaction for BTC to their portfolio
5. A User is able to create multiple Portfolios
6. A User is able to repeat steps 3-4 for new Portfolios


## Scoring Guide

Develop a **Test Plan** for the above product specifications.
Your Test Plan should include **Test Scenarios**, **Test Steps/Cases**, a **Mind Map** and their corresponding pass/fail criteria.


All submissions will be evaluated based on the following criteria: 

- Structure and organization of the Test Plan.
- Conciseness, readability, accuracy and adaptibility to future changes. 
- Test coverage for happy path and edge-case scenarios in critical application components.
- Applied understanding of different testing strategies (equivalence partitions, boundary values, decision table, ).
- Samples of failure scenarios and relevant details e.g. client environment and steps to reproduce.


Example of a mindmap: \
![./img/mindmap.png](./img/mindmap.png)


#### Extra Credit:

L2 and above candidates will additionally be evaluated based on the following scope:
 
- Automated regression testing of the specs 1-4. You should implement this with **Selenium - Java**.  
- This will be assessed based on selectors strategies, code structure and readability, assertions, setup and teardown, test reporting, usage of CICD, and other relevant utilities.


## Submission Guide

- Your submission should include the Test Plan and a completed test execution. You may use [this spreadsheet](https://docs.google.com/spreadsheets/d/1eEzl-cc8pVK4rBaMPmkc2H9CtnJz5fyNGqEy0OUqIOI/) as a starting point. 
- You can use [gitmind.com](https://gitmind.com/) as a free mindmapping software.
- You should fork this repository into a private repository, and upload the (1) Spreadsheet, (2) Mindmap, and (3) Automation Code (if any) into your fork. Invite the users in the [Reviewers](https://gitlab.com/groups/coingecko-dev-test/-/group_members) group with the **Reporter** role.
- We do not expect candidates that meet the baseline to take more than 2 weeks to complete the assignment. Do correspond with the Talent Acquisition team for extensions. 
- Your submission should give us a overview of your depth and breadth of working experience to understand how to help you smoothly onboard onto the team.
- Your submission will be used as a **foundation for the next/final round** of interview. 
- You may use our [stackshare.io](https://stackshare.io/coingecko) profile as a point of reference.
